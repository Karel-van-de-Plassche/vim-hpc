#/bin/sh
./configure \
  --prefix=/usr \
  --localstatedir=/var/lib/vim \
  --with-features=huge \
  --with-compiledby='Arch Linux' \
  --enable-gpm \
  --enable-acl \
  --with-x=no \
  --disable-gui \
  --enable-multibyte \
  --enable-cscope \
  --enable-netbeans \
  --enable-perlinterp=dynamic \
  --enable-pythoninterp=dynamic \
  --enable-python3interp=dynamic \
  --enable-rubyinterp=dynamic \
  --enable-luainterp=dynamic \
  --enable-tclinterp=dynamic \
  --disable-canberra
make
